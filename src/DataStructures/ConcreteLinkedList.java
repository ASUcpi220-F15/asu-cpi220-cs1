package DataStructures;

import DataStructures.ADS;

public class ConcreteLinkedList<T extends Comparable<T>> extends ADS<T> implements ILinkedList<T> {

   private class InternalList {
      public InternalList next;
      public InternalList prev;
      public T value;

      public InternalList() {
         this.value = null;
         this.next = null;
         this.prev = null;
      }

      public String toString() {
         return value.toString();
      }

   }

   private InternalList head;
   private InternalList curr;
   private InternalList tail;
   int pos, len;

   public ConcreteLinkedList(T h, T t) {
      this.head = new InternalList();
      this.head.value = h;
      this.curr = head;
      this.tail = new InternalList();
      this.head.next = this.tail;
      this.tail.prev = this.head;
      this.pos = 0;
      this.len = 0;
   }

   /*
    * Append
    */

   public void append(T value) {

      /* Empty List */
      if (this.head.next == this.tail) {
         this.curr = new InternalList();
         this.head.next = this.curr;
         this.curr.prev = this.head;
         this.curr.next = this.tail;
         this.tail.prev = this.curr;
         this.curr.value = value;
         this.len++;
         return;
      }

      /* > 1 nodes */
      this.tail.prev.next =  new InternalList();
      this.tail.prev.next.prev = this.tail.prev;
      this.tail.prev = this.tail.prev.next;
      this.tail.prev.next = this.tail;
      this.tail.prev.value = value;
      this.len++;
   }

   /*
    * Navigation
    */

   public boolean isLast() {
      return this.curr == this.tail;
   }

   public boolean isFirst() {
      return this.curr == this.head;
   }

   // Note that users are allowed to write to the head 
   // but its not advised
   public boolean next() {
      if (this.curr.next != null) {
         this.curr = this.curr.next;
         this.pos++;
         return true;
      }
      return false;
   }
   // ALLOWS user to write to tail;
   public boolean prev() {
      if (this.curr.prev != null) {
         this.curr = this.curr.prev;
         this.pos--;
         return true;
      }

      return false;
   }

   /* Goes back and forth, repeated sequential calls to seek
    * have the same performance as an array */
   
   public void seek(int i) {
      if (i == this.len) return;

      while (i < this.pos) { //
         if (!this.prev()) break;
      }

      while (i > this.pos) {
         if (!this.next()) break;
      }

   }
   
  
   /*
    * Retrieval
    */
   
   /* seeks and returns current value */
   public T fetch(int i) {
      if (i == this.len) return this.curr.value;
      this.seek(i);
      return this.curr.value;
   }


   public T get() {
      if (this.curr != null)
         return this.curr.value;
      return null;
   }

	public InternalList getNode() {
		 return this.curr;
	}


   public String toString() {
      return this.curr.value.toString();
   }



   public int getLen() {
      return this.len;
   }

   /*
    * Modification
    */

   /* Inserts an element at the beginning */
   /* Used to put a NULL before element 0 in Heapsort */
   public void prepend(T value) {

      /* Empty List */
      if (this.head.next == this.tail) {
         append(value);
         return;
      }

      /* 1 element */
      InternalList tmp = this.head.next;
      InternalList interloper = new InternalList();
      this.head.next = interloper;
      interloper.prev = this.head;
      interloper.next = tmp;
      tmp.prev = interloper;
      interloper.value = value;
      this.len++;
      this.reset();
   }

   public void set(T value) {
      if (this.curr != null)
         this.curr.value = value;

   }

   public void swap(int i, int j) {
      this.seek(i);
      InternalList a = this.curr;
      this.seek(j);
      InternalList b = this.curr;
      
      /* Originally this function moved the pointers around but the amount of data
       * being swapped in this application is negligible so it's fine. 
       */
      
      T tmp;
      tmp = a.value;
      a.value = b.value;
      b.value = tmp;
   }

   public void reset() {
      this.curr = this.head.next;
      this.pos = 0;
   }

   public void dupe(ADS<T> aux, int i, int j) {
      if (i > this.len || j > this.len) return;

      //this.reset(); // MASSIVE BOTTLENECK, AVOID POINTLESS SEEKING !!! 0.24s without 50s with 4 mergesort

      for (int k=i; k <= j; ++k) {
         aux.append(this.fetch(k));
      }

   }
   
   /* Connects the element before the tail in the first list to 
    * to the element after the head in the second list
    */
   public void appendUnsorted(ConcreteLinkedList<T> list) {
      if (list.getLen() == 0) return; /* bail on empty list */
      list.reset();
      InternalList connect = list.getNode();
      this.tail.prev.next = connect;
      connect.prev = this.tail.prev; 
      this.tail.prev = null; // disconnect and remove tail from first list
      this.len += list.getLen();
      this.reset(); /* skip back to beginning */
   }
   
   public static void main(String[] args) {
      
      /* Proof that appendUnsorted works */
      ConcreteLinkedList<Integer> sorted = new ConcreteLinkedList<Integer>(null,null);
      ConcreteLinkedList<Integer> unsorted = new ConcreteLinkedList<Integer>(null,null);
      
      /* Create Sorted */
      for (int i=5; i < 8; ++i)
         sorted.append(i);
      
      /* Create Unsorted */
      unsorted.append(0);
      unsorted.append(1);
      unsorted.append(2);
      
      /* Append Unsorted */
      sorted.appendUnsorted(unsorted);
      
      for (int i=0; i < sorted.getLen(); ++i) {
         System.out.println(sorted.fetch(i));
      }
      
      
      
   }
   

}
