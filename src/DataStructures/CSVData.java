package DataStructures;

public class CSVData implements Comparable<CSVData> {

   public String line;
   private double compare;
   private boolean descend;

   public CSVData(String line, double compare, boolean descend) {
      this.line = line;
      this.compare = compare;
      this.descend = descend;
   }

   public double getCompare() {
      return this.compare;
   }

   @Override
   public int compareTo(CSVData d) {
      if (this.compare == d.getCompare()) return 0;
      if (descend) return (this.compare < d.getCompare()) ? -1 : 1;
      return (this.compare < d.getCompare()) ? 1 : -1;
   }

   public String toString() {
      return this.line;
   }

}
