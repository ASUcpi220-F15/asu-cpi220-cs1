package DataStructures;

public interface ILinkedList<T> {

   public boolean next();
   public boolean prev();
   public T get();

}