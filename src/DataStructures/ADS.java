package DataStructures;

public abstract class ADS<T> {

   public abstract void append(T value);
   public abstract void prepend(T value);
   public abstract boolean next();
   public abstract boolean prev();
   public abstract int getLen();
   public abstract void seek(int i);
   public abstract void swap(int i, int j);
   public abstract void set(T value);
   public abstract void dupe(ADS<T> aux, int i, int j);
   public abstract void reset();
   public abstract boolean isLast();
   public abstract T get();
   public abstract T fetch(int i);

}
