package edu.asu.sorting;

import DataStructures.ADS;
import DataStructures.ConcreteLinkedList;
import java.util.Random;
import util.Stopwatch;

public class ADS_Sorters<T extends Comparable<T>> {

   
  //  _   _                                 _   
  // | | | | ___  __ _ _ __  ___  ___  _ __| |_ 
  // | |_| |/ _ \/ _` | '_ \/ __|/ _ \| '__| __|
  // |  _  |  __/ (_| | |_) \__ \ (_) | |  | |_ 
  // |_| |_|\___|\__,_| .__/|___/\___/|_|   \__|
  //                  |_|                       
  // 
  // Implementation borrowed from Algorithms Textbook
   
   private boolean less(ADS<T> data, int i, int j) {
      T a = data.fetch(i);
      T b = data.fetch(j);
      return a.compareTo(b) < 0;
   }

   private void exch(ADS<T> data, int i, int j) {
      T key = data.fetch(i);
      data.swap(i, j);
      data.seek(j);
      data.set(key);
   }

   private void sink(ADS<T> data, int k, int n) {
      while (2 * k <= n) {
         int j = 2 * k;
         if (j < n && less(data, j, j + 1))
            j++;
         if (!less(data, k, j))
            break;
         exch(data, k, j);
         k = j;
      }
   }
   
   public void heapsort(ADS<T> data) {
      
      /* This sorting algorithm runs especially slow on my ADS data structure
       * because it tries to access every element as an array but the ADS
       * data structure used in this assignment (Doubly Linked List) can't
       * provide O(1) access. Causing it to run incredibly slowly as it has 
       * to seek back and forth. This stack overflow post speculates that
       * it would cause the runtime to be on the order of O(n²log(n)) !
       * http://stackoverflow.com/a/10885753
       */

      /* Every parent is k/2 */
      /* Every child is 2k and 2k+1 respectively */

      data.prepend(null);
      data.seek(1);

      int n = data.getLen() - 1;

      /* Construct Heap */
      for (int k = n / 2; k >= 1; k--)
         sink(data, k, n);

      /* Sort heap */
      while (n > 1) {
         data.swap(1, n--);
         sink(data, 1, n);
      }

   }
   
  //  __  __                                     _   
  // |  \/  | ___ _ __ __ _  ___  ___  ___  _ __| |_ 
  // | |\/| |/ _ \ '__/ _` |/ _ \/ __|/ _ \| '__| __|
  // | |  | |  __/ | | (_| |  __/\__ \ (_) | |  | |_ 
  // |_|  |_|\___|_|  \__, |\___||___/\___/|_|   \__|
  //                  |___/                          
  // Average: O(n log(n))
  // Worst:   O(n log(n))
        
   /*
    * I took the following two methods from the MergeBU implementation
    * in the textbook and adapted the first one to suit my needs. Instead of 
    * incrementing i and j where i corresponds to the left half of the array
    * and j the right half, I created two auxiliary LinkedLists called a left
    * and right and simply copy the contents of the data array to left and the right
    * for the specific subarray. Then I can iterate through each left and right until
    * the tail is found and do the appropriate data movements . This allows the algorithm to 
    * be more intuitive to read and and works really well with the linked list. Pairs are 
    * accessed sequentially which means the linked list works as well as the array as it doesn't
    * have any unnesscary seeking back and forth. 
    */
   
   public void merge(ADS<T> data, int lo, int mid, int hi) {
      ADS<T> left = new ConcreteLinkedList<T>(null, null);
      ADS<T> right = new ConcreteLinkedList<T>(null, null);
      data.dupe(left, lo, mid);
      data.dupe(right, mid + 1, hi);

      /* O(1), just puts it back to the HEAD */
      left.reset();
      right.reset();

      int k = lo;
      while (true) {

         /* same performance as array */
         data.seek(k);
         T lvalue = left.get();
         T rvalue = right.get();

         /* LEFT EXHAUSTED, FILL w/ RIGHT && BAIL */
         if (left.isLast()) {
            if (right.isLast())
               return;
            data.set(rvalue);
            if (right.next() == false)
               return;
         }

         /* RIGHT EXAHUSTED, FILL w/ LEFT && BAIL */
         else if (right.isLast()) {
            if (left.isLast())
               return;
            data.set(lvalue);
            if (left.next() == false)
               return;
         }

         /* RIGHT < LEFT; PUT RIGHT IN */
         else if (rvalue.compareTo(lvalue) < 0) {
            data.set(rvalue);
            right.next();
         }

         /* LEFT < RIGHT; PUT LEFT IN */
         else {
            data.set(lvalue);
            left.next();
         }

         ++k;
      }
   }
   
   // taken from Algorithms textbook
   public void mergesort(ADS<T> data) {
      if (data == null || data.getLen() <= 1) return;
      
      int n = data.getLen();
      for (int sz = 1; sz < n; sz = sz + sz) {
         for (int lo = 0; lo < n - sz; lo += sz + sz) {
            merge(data, lo, lo + sz - 1, Math.min(lo + sz + sz - 1, n - 1));
         }

      }

   }
   
   // _____ _                            _   
   // |  ___(_)_ __  _ __  ___  ___  _ __| |_ 
   // | |_  | | '_ \| '_ \/ __|/ _ \| '__| __|
   // |  _| | | | | | | | \__ \ (_) | |  | |_ 
   // |_|   |_|_| |_|_| |_|___/\___/|_|   \__|
   // 
   // (aka Garbgesort)
   // Hilariously Slow Selection Sort Variant, adapted with ADS support 
   // Best: O(n)
   // Worst: O(n³)
   

   public void finnsort(ADS<T> data) {
      if (data == null || data.getLen() <= 1) return;
      
      int count = 0;
      while(!isSorted(data)) {

         int toBeSwitched = findToBeSwitched(data);
         for(int y = count; y < data.getLen(); y++) {
            if (data.fetch(toBeSwitched).compareTo(data.fetch(y)) > 0 ) {
               toBeSwitched = y;
            }
         }

         data.swap(count++, toBeSwitched);
      }
   }
   
   private int findToBeSwitched(ADS<T> data) {
      if (data.getLen() <= 1) return 0;

      int i;
      int selection = data.getLen() - 1;
      
      for(i = 1; i < data.getLen(); i++) {
         if (data.fetch(i-1).compareTo(data.fetch(i)) > 0 ) {
            selection = i-1;
         }
      }
      
      return selection;
   }

   private boolean isSorted(ADS<T> data) {
      if (data.getLen() <= 1) return true;
      boolean isSorted = true;
      
      for (int i=1; i < data.getLen(); ++i)
         if (data.fetch(i-1).compareTo(data.fetch(i)) > 0 )
            isSorted = false;


      return isSorted;
   }
   
  //  ____       _           _   _               ____             _   
  // / ___|  ___| | ___  ___| |_(_) ___  _ __   / ___|  ___  _ __| |_ 
  // \___ \ / _ \ |/ _ \/ __| __| |/ _ \| '_ \  \___ \ / _ \| '__| __|
  //  ___) |  __/ |  __/ (__| |_| | (_) | | | |  ___) | (_) | |  | |_ 
  // |____/ \___|_|\___|\___|\__|_|\___/|_| |_| |____/ \___/|_|   \__|
  //
  // Improved Normal Version of Selection Sort / Finnsort
  // Average: O(n²)
  // Worst: O(n²)
                                                                   
 
   public void selection(ADS<T> data) {
      if (data == null || data.getLen() <= 1) return;

      int len = data.getLen();
      int swap = -1;
      T min = data.fetch(0);
      T val = null;
      int wall = 0;

      while (wall < len) {
         swap = -1;
         min = data.fetch(wall);
         for (int i = wall; i < len; ++i) {
            val = data.fetch(i);
            if (val.compareTo(min) <= 0) {
               min = val;
               swap = i;
            }
         }

         if (swap == -1)
            break; // no more swapping to do
         data.swap(wall, swap);
         wall++;

      }

   }
   
  //  _____         _       
  // |_   _|__  ___| |_ ___ 
  //   | |/ _ \/ __| __/ __|
  //   | |  __/\__ \ |_\__ \
  //   |_|\___||___/\__|___/
  //
  // Small testing method to test all methods in ADS_Sorters

  public static void main(String[] args) {
     
     ConcreteLinkedList<Integer>    random = new ConcreteLinkedList<Integer>(null,null);
     ConcreteLinkedList<Integer>    sorted = new ConcreteLinkedList<Integer>(null,null);
     ConcreteLinkedList<Integer>   reverse = new ConcreteLinkedList<Integer>(null,null);
     ConcreteLinkedList<Integer>     empty = new ConcreteLinkedList<Integer>(null,null);
     Random r = new Random();
     int fergalicious = 1000;
     
     System.out.printf("Sorting %d elements\n", fergalicious);
     
     /* Random */
     for (int i=fergalicious; i > 0; --i)
        random.append(r.nextInt(fergalicious));
     
     /* Sorted Data */
     for (int i=0; i < fergalicious; ++i)
        sorted.append(i);
     
     /* Reverse */
     for (int i=fergalicious; i > 0; --i) {
        reverse.append(i);
     }
     
     ADS_Sorters <Integer> sort = new ADS_Sorters<Integer>();
     Stopwatch s = new Stopwatch();
     
     sort.mergesort(random);
     sort.mergesort(sorted);
     sort.mergesort(reverse);
     sort.mergesort(empty);
     sort.mergesort(null);
     sort.selection(random);
     sort.selection(sorted);
     sort.selection(reverse);
     sort.selection(empty);
     sort.selection(null);
     double time = s.elapsedTime();
     System.out.printf("Took %f, nothing crashed ✓", time);
     
  }

}
