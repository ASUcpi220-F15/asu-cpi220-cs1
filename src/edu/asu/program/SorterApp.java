package edu.asu.program;
import DataStructures.CSVData;
import DataStructures.ConcreteLinkedList;
import edu.asu.data.CSV;
import edu.asu.sorting.ADS_Sorters;
import java.io.File;
import util.Stopwatch;

public class SorterApp {

   public static void main(String[] args) {
      Stopwatch time = new Stopwatch();

      /* CSV Entry Key
       * -----------------------
       * title                0
       * artist_name,         1
       * mood,                2
       * genre,               3
       * energy               4
       * liveness             5
       * tempo                6
       * speechiness          7
       * acousticness         8
       * danceability         9
       * instrumentalness     10
       * loudness             11
       */

      /* CSV Entry Information */
      CSV file = new CSV("playlist.csv");
      CSV[] fouts = new CSV[6];
      String[] moods = { "contemplative", "active", "chilling", "nightlife", "motivation", "roadtrip" };
      int[] key  = {11, 4, 5, 9, 8, 7};
      boolean[] descend = {true,false,true,false,false,true};
      ConcreteLinkedList<CSVData> list = new ConcreteLinkedList<CSVData>(null,null);
      ADS_Sorters<CSVData> sort = new ADS_Sorters<CSVData>();

      int cid = 0; // current mood id, matches it up with the above arrays for settings
     
      System.out.println(">>> Starting Sorting");
      while (cid < moods.length) {
         String line = file.getEntry();
         String[] entry = null;

         if (line != null)
            entry = line.split(",");

         /* Sort, Write, Continue */
         if (entry == null) {
            System.out.printf(">>> Sorting %s\n", moods[cid]);
            sort.mergesort(list); /* change algorithm here: OPTIONS .heapsort .selection .finnsort .mergesort */
            fouts[cid] = new CSV(moods[cid] + ".csv");
            fouts[cid].write(list);
            list = null; /* garbage collection == my savior */
            list = new ConcreteLinkedList<CSVData>(null,null);
            file.reset();
            cid++;
         }

         /* Add Entry */
         else if (entry != null && entry.length >= 5 && entry[2].equalsIgnoreCase(moods[cid])) {
            try {
               CSVData data = new CSVData(line, Double.parseDouble(entry[key[cid]]), descend[cid]);
               list.append(data);
            } catch (Exception e) {}
         }
      }

     System.out.println(">>> Concatanating Sorted Lists");
     
     try {
        /* Delete Previous Sorted List */
        File f = new File("sorted.csv");
        f.delete();
     } catch (Exception e) {}
     
     CSV.concat(fouts, "sorted.csv");
     
     System.out.println(">>> Deleting Temporary Lists");
     
     for (String path: moods)
       try {
          File f = new File(path + ".csv");
          f.delete();
      } catch (Exception e) {}
     
	  System.out.println("✓ Everything Peachy Keen");
	  System.out.printf("Time: %f\n", time.elapsedTime());
   }
}
