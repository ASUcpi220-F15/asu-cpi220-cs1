package edu.asu.data;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import DataStructures.ConcreteLinkedList;

public class CSV {

   private RandomAccessFile fp;

   public CSV(String fname) {
      try {
         this.fp = new RandomAccessFile(fname, "rw");
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }
   }

   /* Reading */
   public String getEntry() {
      String s = null;
      try {
         s = this.fp.readLine();
      } catch (Exception e) {
         e.printStackTrace();
      }
      return (s == null) ? null : s;
   }

   public void reset() {
      try {
         this.fp.seek(0);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   /*
    * Writing
    */

   public void write(ConcreteLinkedList<?> list) {
		for (int i=0; i < list.getLen(); ++i)
		try {
		   if (list.fetch(i) != null)
			this.fp.writeBytes(list.fetch(i).toString() + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.reset();
   }

   /*
    * Static Utility Methods
    */

   public static void concat(CSV[] files, String output) {

      RandomAccessFile master = null;

      try {
         master = new RandomAccessFile(output, "rw");;
      } catch (Exception e) {
         e.printStackTrace();
		 return;
      }

	   for (CSV f: files) {
		   try {
			   f.reset();
			   while (true) {
				   String s =  f.getEntry();
				   if (s == null) break;
				   master.writeBytes(s + "\n");
			   }
			   f.close();
		   } catch (Exception e) {}
	   }
	   
	   try {
	      master.close();
	   } catch (Exception e) {}
	   
   }

   public void close() {
      try {
         this.fp.close();
      } catch (Exception e) {}
   }

}
