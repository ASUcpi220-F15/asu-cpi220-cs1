package edu.asu.data;
import java.util.Random;

public class DataGenerator {

   public static int[] generateRandomIntArray(int size) {
      int[] data = new int[size];
      Random r = new Random();
      for(int i = 0; i < size; i++) {
         data[i] = r.nextInt(size);
      }
      return data;
   }

   public static int[] generateReverseIntArray(int size) {
      int[] data = new int[size];
      for(int i = 0; i < size; i++) {
         data[i] = size--;
      }
      return data;
   }

   public static int[] generateIntArray(int size) {
      int[] data = new int[size];
      for(int i = 0; i < size; i++) {
         data[i] = i;
      }
      return data;
   }

}
