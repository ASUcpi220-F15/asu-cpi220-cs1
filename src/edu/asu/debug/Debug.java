package edu.asu.debug;
import DataStructures.ILinkedList;

public class Debug {

   public static void printArray(int[] data, int sindex) {
      String event = "";
      System.out.print("[");
      for (int i=0; i < data.length; ++i) {
         event = (i+1 < data.length) ? "," : "";
         event = (i == sindex && i != -1) ? "|" : event;
         System.out.printf("%d%s", data[i],	event);
      }
      System.out.print("]\n");
   }

   public static void printList(ILinkedList<?> node) {
      while (true) {
         if (node == null) break;
         System.out.println(node);
         node.next();
      }
   }

}
