package util;

// http://introcs.cs.princeton.edu/java/stdlib/Stopwatch.java.html
// this class is like 5 lines and ezpz but referencing the source anyway
// to be on the safe side

public class Stopwatch { 

    private final long start;
    
    public Stopwatch() {
        start = System.currentTimeMillis();
    } 
    
    public double elapsedTime() {
        long now = System.currentTimeMillis();
        return (now - start) / 1000.0;
    }

} 
